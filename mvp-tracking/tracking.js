/*
Google Sheets Script used to track Boss Monsters in the MMORPG Ragnarok Online
Private server LimitRO using a Google Sheets template.

A user edits the LOC column on the sheet with a clock position describing the
general area where the boss monster was defeated within the map

The script then automatically:
(1) calculates the timeframe when the boss may respawn and edits corresponding
columns on the sheet with relevant information
(2) sends an embedded message to a specified Discord text channel containing
the boss name, anticipated respawn window in unix timestamp markdown, and other
relevant information, using Webhooks
(3) creates a trigger for a delayed function call corresponding to the
anticipated respawn window
(4) executes the triggered function to send an announcement that the respawn window
timeframe has been reached, using Webhooks

NOTE: Google Apps Scripts limitations include a defined number of triggers that
can be created

Copyright Martey Haw Apr 2022
*/

// Setup Variables
var EditTriggerAutoSort = "B2:C51"; // Range that will trigger the autosort when edited
var ApplicableSheetName = "LIMIT 4TH JOB"; // Name of the sheet we want to apply the script to
var TableRangetosort = "A2:L51"; // Range in the sheet we want auto sorted
var SortByThisColumn = "C"; // Column we want the autosort to sort using
var MVPColumn = "A"; // Column letter containing LOC
var LOCColumn = "B"; // Column letter containing LOC
var TimeColumn = "C"; // Column letter containing Time
var DelayColum = "D"; // Column letter containing Delay in Hours
var FullDelayColum = "E"; // Column letter containing Full Delay
var WeaknessColum = "F"; // Column letter containing Full Delay
var MapUpdateColumn = "I"; // Column letter containing Updated Map checkbox
var PostedDateColumn = "J"; // Column letter containing Posted Date
var ExpectedDateColumn = "K"; // Column letter containing Expected Date
var GIFImageColumn = "L"; // Column letter containing Expected Date
var ServerTimezone = "Etc/GMT"; // Server's Timezone. Refer to Timezone IDs. https://developers.google.com/google-ads/api/reference/data/codes-formats#timezone-ids
var TimeColumnFormat = "HH:mm:ss"; // TimeColumn's Time Format. https://developers.google.com/sheets/api/guides/formats#date_and_time_format_patterns
var PostedAndExpectedDateFormat = "dd/MM/yyyy"; // Posted/Expected Date Format. https://developers.google.com/sheets/api/guides/formats#date_and_time_format_patterns
var POST_URL = ""; // Webhook URL for instantaneous reminder
var POST_URL2 = ""; // Webhook URL for delayed reminder

/* Conversion from Letter to Column Number */
var MVPColumnNo = letterToColumn(MVPColumn);
var LOCColumnNo = letterToColumn(LOCColumn);
var TimeColumnNo = letterToColumn(TimeColumn);
var DelayColumNo = letterToColumn(DelayColum);
var FullDelayColumNo = letterToColumn(FullDelayColum);
var WeaknessColumNo = letterToColumn(WeaknessColum);
var PostedDateColumnNo = letterToColumn(PostedDateColumn);
var ExpectedDateColumnNo = letterToColumn(ExpectedDateColumn);
var GIFImageColumnNo = letterToColumn(GIFImageColumn);
var MapUpdateColumnNo = letterToColumn(MapUpdateColumn);
var ColumnToSortBy = letterToColumn(SortByThisColumn);

/* FOR NEW USERS TO AUTHORIZE THE SCRIPT */
function init() {
  SpreadsheetApp.getActiveSpreadsheet().toast("Authorized", "Initialization:");
}

function onOpen() {
  Browser.msgBox("Welcome!");

  var ui = SpreadsheetApp.getUi();
  ui.createMenu("Maintenance")
    .addItem("Clear MVP Time", "clearContent")
    .addItem("Initialize", "init")
    .addToUi();
}

function clearContent() {
  var ss = SpreadsheetApp.getActiveSpreadsheet();
  var sheet = ss.getSheets()[0];
  sheet.getRange("B2:C49").clearContent();
  sheet.getRange("N17:P36").clearContent();
}

/* COLUMN/LETTER CONVERTER FUNCTIONS */
function columnToLetter(column) {
  var temp,
    letter = "";
  while (column > 0) {
    temp = (column - 1) % 26;
    letter = String.fromCharCode(temp + 65) + letter;
    column = (column - temp - 1) / 26;
  }
  return letter;
}

function letterToColumn(letter) {
  var column = 0,
    length = letter.length;
  for (var i = 0; i < length; i++) {
    column += (letter.charCodeAt(i) - 64) * Math.pow(26, length - i - 1);
  }
  return column;
}

/* ON EDIT TRIGGER FUNCTION */
function onEdit(e) {
  // Check if the user edited within the applicable sheet
  if (e.source.getSheetName() == ApplicableSheetName) {
    var AutoSortTriggerRange =
      SpreadsheetApp.getActiveSheet().getRange(EditTriggerAutoSort);
    var row = e.range.getRow();
    var col = e.range.getColumn();

    // Check if the edited column/row is within the set trigger range
    if (
      col >= AutoSortTriggerRange.getColumn() &&
      col <= AutoSortTriggerRange.getLastColumn() &&
      row >= AutoSortTriggerRange.getRow() &&
      row <= AutoSortTriggerRange.getLastRow()
    ) {
      var sheet = e.source.getActiveSheet();
      var editedCell = sheet.getActiveCell();
      var editedCellRow = editedCell.getRow();

      // Check if the edited column is the LOC column
      if (editedCell.getColumn() == letterToColumn(LOCColumn)) {
        // Check if the user emptied the cell
        if (editedCell.getValues() == "") {
          sheet.getRange(editedCellRow, TimeColumnNo).setValue("");
          sheet.getRange(editedCellRow, MapUpdateColumnNo).setValue("");
          sheet.getRange(editedCellRow, PostedDateColumnNo).setValue("");
          sheet.getRange(editedCellRow, ExpectedDateColumnNo).setValue("");
          SpreadsheetApp.flush();
        }
        // Otherwise if the user did not empty the cell, then update Time withe delay, reset MapUpdate, and time stamp for Posted and Expected dates
        else {
          var hours = sheet.getRange(editedCellRow, DelayColumNo).getValues();
          var now = new Date();
          var nowInMS = now.getTime() + Number(hours) * 60 * 60 * 1000;
          var delayedTime = new Date(nowInMS);
          var delayedTimeUnix = Math.floor(
            new Date(nowInMS).getTime() / 1000
          ).toString();

          var mvpname = sheet.getRange(editedCellRow, MVPColumnNo).getValue();
          var delayrange = sheet
            .getRange(editedCellRow, FullDelayColumNo)
            .getValue();
          var mvpweakness = sheet
            .getRange(editedCellRow, WeaknessColumNo)
            .getValue();
          var mvpLoc = sheet
            .getRange(editedCellRow, LOCColumnNo)
            .getDisplayValue();
          var GIFImage = sheet
            .getRange(editedCellRow, GIFImageColumnNo)
            .getValue();
          var colorhex = Math.floor(Math.random() * 16777214) + 1;

          sheet
            .getRange(editedCellRow, TimeColumnNo)
            .setValue(
              Utilities.formatDate(
                delayedTime,
                ServerTimezone,
                TimeColumnFormat
              )
            );
          sheet.getRange(editedCellRow, MapUpdateColumnNo).setValue("");
          sheet
            .getRange(editedCellRow, PostedDateColumnNo)
            .setValue(
              Utilities.formatDate(
                new Date(),
                ServerTimezone,
                PostedAndExpectedDateFormat
              )
            );
          sheet
            .getRange(editedCellRow, ExpectedDateColumnNo)
            .setValue(
              Utilities.formatDate(
                delayedTime,
                ServerTimezone,
                PostedAndExpectedDateFormat
              )
            );
          SpreadsheetApp.flush();

          var options = {
            muteHttpExceptions: true,
            method: "post",
            headers: {
              "Content-Type": "application/json",
            },
            payload: JSON.stringify({
              content: "‌",
              embeds: [
                {
                  title: mvpname,
                  color: colorhex,
                  description:
                    "Respawn **<t:" +
                    delayedTimeUnix +
                    ":R>** \n[" +
                    delayrange +
                    "] \nWeak to: **" +
                    mvpweakness +
                    "** \nLOC: **" +
                    mvpLoc +
                    "**",
                  image: { url: GIFImage },
                },
              ],
            }),
          };
          UrlFetchApp.fetch(POST_URL, options);

          var assembleMVPentry = [
            parseInt(delayedTimeUnix),
            mvpname,
            colorhex,
            mvpLoc,
          ];

          var reversejarray =
            PropertiesService.getScriptProperties().getProperty(
              "mvpQueueArray"
            );
          if (!reversejarray) {
            reversejarray = [];
            reversejarray = JSON.stringify(reversejarray);
          }

          var mvpQueueArray = JSON.parse(reversejarray);

          mvpQueueArray.push(assembleMVPentry);

          mvpQueueArray = mvpQueueArray.sort(function (a, b) {
            return a[0] - b[0];
          });

          var jarray = JSON.stringify(mvpQueueArray);
          PropertiesService.getScriptProperties().setProperty(
            "mvpQueueArray",
            jarray
          );

          var time_af = Math.abs(delayedTime - now) / 36e5;
          console.log(time_af);

          ScriptApp.newTrigger("announceRespawn")
            .timeBased()
            .after(time_af * 60 * 60 * 1000)
            .create();
        }

        // In either case of emptying cell, or editing to anything else, trigger the autosort
        var range = sheet.getRange(TableRangetosort);
        range.sort({ column: ColumnToSortBy });
        SpreadsheetApp.flush();
      }

      // Check if the edited column is the TIME column
      if (editedCell.getColumn() == TimeColumnNo) {
        // Check if the user deleted the TIME column entry, if so, reset LOC, Map Updated, Posted, and Expected dates
        if (editedCell.getValues() == "") {
          sheet.getRange(editedCellRow, LOCColumnNo).setValue("");
          sheet.getRange(editedCellRow, MapUpdateColumnNo).setValue("");
          sheet.getRange(editedCellRow, PostedDateColumnNo).setValue("");
          sheet.getRange(editedCellRow, ExpectedDateColumnNo).setValue("");
          SpreadsheetApp.flush();
        }

        // Otherwise if the user edited the TIME column, trigger autosort
        var range = sheet.getRange(TableRangetosort);
        range.sort({ column: ColumnToSortBy });
        SpreadsheetApp.flush();
      }
    }
  }
}

/* ANNOUNCE MVP RESPAWN WINDOW */
function announceRespawn(event) {
  var reversejarray =
    PropertiesService.getScriptProperties().getProperty("mvpQueueArray");
  if (!reversejarray) {
    reversejarray = [];
    reversejarray = JSON.stringify(reversejarray);
  }
  var retrievemvpQueueArray = JSON.parse(reversejarray);

  var mvpsname = retrievemvpQueueArray[0][1];
  var colorshex = retrievemvpQueueArray[0][2];
  var mvpsLoc = retrievemvpQueueArray[0][3];

  retrievemvpQueueArray = retrievemvpQueueArray.slice(1);

  var jarray = JSON.stringify(retrievemvpQueueArray);
  PropertiesService.getScriptProperties().setProperty("mvpQueueArray", jarray);

  var options2 = {
    muteHttpExceptions: true,
    method: "post",
    headers: {
      "Content-Type": "application/json",
    },
    payload: JSON.stringify({
      embeds: [
        {
          color: colorshex,
          description: "**" + mvpsname + " has entered the delay window.**",
        },
      ],
    }),
  };
  UrlFetchApp.fetch(POST_URL2, options2);

  try {
    var triggerUid = event.triggerUid;
    var triggers = ScriptApp.getProjectTriggers();
    if (triggers && triggers.length > 0) {
      triggers.forEach(function (item) {
        if (item.getUniqueId() == triggerUid) {
          ScriptApp.deleteTrigger(item);
        }
      });
    }
  } catch (err) {
    console.log(err);
  }
}

/* DEV FUNCTION TO RESET MVP QUEUE ARRAY */
function resetProperties() {
  PropertiesService.getScriptProperties().setProperty("mvpQueueArray", "");
}

/* DEV FUNCTION TO CHECK THE CURRENT MVP QUEUE ARRAY */
function checkmvpQueueArray() {
  //Retrieve the global Array from the script properties
  var reversejarray =
    PropertiesService.getScriptProperties().getProperty("mvpQueueArray");
  console.log(reversejarray);

  //If the global Array is null or undefined, create new empty array, then JSON.stringify to counter parse JSON.parse
  if (!reversejarray) {
    reversejarray = [];
    reversejarray = JSON.stringify(reversejarray);
  }
  //If the global Array passes the check, then assign global Array to the mvpQueueArray
  var retrievemvpQueueArray = JSON.parse(reversejarray);

  console.log(retrievemvpQueueArray);
}
