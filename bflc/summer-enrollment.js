/*
Google Sheets Script used to log Student Program Enrollment
information from a Form template page into a Sheet database
Copyright Martey Haw Jan 2015
*/

function dataSubmit() {
  /* INITIAL VARIABLE SETUP */
  var templatename = "Sheet1";
  var datapagename = "Sheet2";

  var ui = SpreadsheetApp.getUi();
  var result = ui.alert(
    "Please Confirm",
    "Are you sure you want to submit?",
    ui.ButtonSet.YES_NO
  );
  if (result == ui.Button.YES) {
    /* VARIABLE SETUP */
    var timestmp = new Date(); // Timestamp
    var grid = SpreadsheetApp.getActiveSpreadsheet()
      .getSheetByName(templatename)
      .getRange(1, 1, 109, 13)
      .getValues(); // Extract info
    var firstn = grid[4][2]; // First name
    var lastn = grid[4][7]; // Last name
    var birthd = grid[6][2]; // DoB
    var newstdnt = grid[6][8]; // New student or not
    var grinfall = grid[8][2]; // Grade level in the fall
    var curhwc = grid[8][8]; // Currently in Homework Club or not
    var paymethd = grid[58][4]; // Payment method

    /* COLLECT WEEKS AND PROGRAMS SIGNUP */
    var lw1 = grid[37][4];
    var lw2 = grid[38][4];
    var lw3 = grid[39][4];
    var lw4 = grid[40][4];
    var lw5 = grid[41][4];
    var lw6 = grid[42][4];
    var lw7 = grid[43][4];
    var lw8 = grid[44][4];
    var lw9 = grid[45][4];
    var lw10 = grid[46][4];
    var lpuw1 = grid[37][6];
    var lpuw2 = grid[38][6];
    var lpuw3 = grid[39][6];
    var lpuw4 = grid[40][6];
    var lpuw5 = grid[41][6];
    var lpuw6 = grid[42][6];
    var lpuw7 = grid[43][6];
    var lpuw8 = grid[44][6];
    var lpuw9 = grid[45][6];
    var lpuw10 = grid[46][6];

    /* PROGRAM NAMES */
    var program1 = grid[12][4] + " " + grid[13][4] + " " + grid[14][4];
    var program2 = grid[12][6] + " " + grid[13][6] + " " + grid[14][6];
    var program3 = grid[12][8] + " " + grid[13][8] + " " + grid[14][8];
    var program4 = grid[12][10] + " " + grid[13][10] + " " + grid[14][10];

    /* RECORD TO DATABASE SHEET */
    var sheet = SpreadsheetApp.getActiveSheet();
    var startRow = 19;
    var numRows = 28;
    var dataRange = sheet.getRange(startRow, 5, numRows, 11);
    var data = dataRange.getValues();
    var datapage =
      SpreadsheetApp.getActiveSpreadsheet().getSheetByName(datapagename);
    var rowtowritedata = datapage.getLastRow() + 1;

    /* CHECKING FOR BLANKS */
    var blnkchk = 0;
    if (firstn == 0) {
      var blnkchk = blnkchk + 1;
    }
    if (lastn == 0) {
      var blnkchk = blnkchk + 1;
    }
    if (birthd == 0) {
      var blnkchk = blnkchk + 1;
    }
    if (newstdnt == 0) {
      var blnkchk = blnkchk + 1;
    }
    if (grinfall == 0) {
      var blnkchk = blnkchk + 1;
    }
    if (curhwc == 0) {
      var blnkchk = blnkchk + 1;
    }

    if (blnkchk != 0) {
      ui.alert("You have " + blnkchk + " empty entries in Student Information");
    } else {
      for (var i = 0; i < data.length; ++i) {
        var row = data[i];
        var chkprog1 = row[0];
        var chkprog2 = row[2];
        var chkprog3 = row[4];
        var chkprog4 = row[6];

        if (chkprog1 != 0) {
          datapage.getRange(rowtowritedata, 8 + i).setValue(program1);
          sheet.getRange(startRow + i, 5).setValue("");
          SpreadsheetApp.flush();
        }

        if (chkprog2 != 0) {
          datapage.getRange(rowtowritedata, 8 + i).setValue(program2);
          sheet.getRange(startRow + i, 7).setValue("");
          SpreadsheetApp.flush();
        }

        if (chkprog3 != 0) {
          datapage.getRange(rowtowritedata, 8 + i).setValue(program3);
          sheet.getRange(startRow + i, 9).setValue("");
          SpreadsheetApp.flush();
        }

        if (chkprog4 != 0) {
          datapage.getRange(rowtowritedata, 8 + i).setValue(program4);
          sheet.getRange(startRow + i, 11).setValue("");
          SpreadsheetApp.flush();
        }
      }

      // Writing Data
      datapage.getRange(rowtowritedata, 1).setValue(timestmp);
      datapage.getRange(rowtowritedata, 2).setValue(firstn);
      datapage.getRange(rowtowritedata, 3).setValue(lastn);
      datapage.getRange(rowtowritedata, 4).setValue(birthd);
      datapage.getRange(rowtowritedata, 5).setValue(newstdnt);
      datapage.getRange(rowtowritedata, 6).setValue(grinfall);
      datapage.getRange(rowtowritedata, 7).setValue(curhwc);
      datapage.getRange(rowtowritedata, 18).setValue(lw1);
      datapage.getRange(rowtowritedata, 19).setValue(lw2);
      datapage.getRange(rowtowritedata, 20).setValue(lw3);
      datapage.getRange(rowtowritedata, 21).setValue(lw4);
      datapage.getRange(rowtowritedata, 22).setValue(lw5);
      datapage.getRange(rowtowritedata, 23).setValue(lw6);
      datapage.getRange(rowtowritedata, 24).setValue(lw7);
      datapage.getRange(rowtowritedata, 25).setValue(lw8);
      datapage.getRange(rowtowritedata, 26).setValue(lw9);
      datapage.getRange(rowtowritedata, 27).setValue(lw10);
      datapage.getRange(rowtowritedata, 28).setValue(lpuw1);
      datapage.getRange(rowtowritedata, 29).setValue(lpuw2);
      datapage.getRange(rowtowritedata, 30).setValue(lpuw3);
      datapage.getRange(rowtowritedata, 31).setValue(lpuw4);
      datapage.getRange(rowtowritedata, 32).setValue(lpuw5);
      datapage.getRange(rowtowritedata, 33).setValue(lpuw6);
      datapage.getRange(rowtowritedata, 34).setValue(lpuw7);
      datapage.getRange(rowtowritedata, 35).setValue(lpuw8);
      datapage.getRange(rowtowritedata, 36).setValue(lpuw9);
      datapage.getRange(rowtowritedata, 37).setValue(lpuw10);
      datapage.getRange(rowtowritedata, 38).setValue(paymethd);

      //Clearing data
      sheet.getRange(5, 3).setValue("");
      sheet.getRange(5, 8).setValue("");
      sheet.getRange(7, 3).setValue("");
      sheet.getRange(7, 9).setValue("");
      sheet.getRange(9, 3).setValue("");
      sheet.getRange(9, 9).setValue("");
      sheet.getRange(38, 5).setValue("");
      sheet.getRange(39, 5).setValue("");
      sheet.getRange(40, 5).setValue("");
      sheet.getRange(41, 5).setValue("");
      sheet.getRange(42, 5).setValue("");
      sheet.getRange(43, 5).setValue("");
      sheet.getRange(44, 5).setValue("");
      sheet.getRange(45, 5).setValue("");
      sheet.getRange(46, 5).setValue("");
      sheet.getRange(47, 5).setValue("");
      sheet.getRange(38, 7).setValue("");
      sheet.getRange(39, 7).setValue("");
      sheet.getRange(40, 7).setValue("");
      sheet.getRange(41, 7).setValue("");
      sheet.getRange(42, 7).setValue("");
      sheet.getRange(43, 7).setValue("");
      sheet.getRange(44, 7).setValue("");
      sheet.getRange(45, 7).setValue("");
      sheet.getRange(46, 7).setValue("");
      sheet.getRange(47, 7).setValue("");
      sheet.getRange(59, 5).setValue("");

      // Use either Toast pop-up or UI alert pop-up
      // SpreadsheetApp.getActiveSpreadsheet().toast("Thank You!","Data Submitted");
      ui.alert("Data Submitted");
    }
  } else {
    ui.alert("Sending cancelled.");
  }
}
