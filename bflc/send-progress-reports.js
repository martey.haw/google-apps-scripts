/*
Google Sheets Script used to automate sending personalized
Student Progress Reports (emailed HTML) with unique and
individual Teacher feedback for the English Program at
Brighter Future Learning Center using Google Sheets as
both a template and database.

Sessions are approximately 2 months with about 4 sessions
per year and progress reports are sent every mid-session
and end-of-sesssion (once a month)

Copyright Martey Haw Jan 2015
*/

/* INITIAL VARIABLE SETUP */
var templatename = "MSLRTemplate";
// var templatename2 = "EoSLRTemplate";
var S1MidSentCol = 12;
var S2MidSentCol = 23;
var S3MidSentCol = 38;
var S4MidSentCol = 49;
var S5MidSentCol = 60;

/* APPEND CUSTOM DROPDOWN MENU TO UI */
/* CURRENTLY ON SESSION 3 */
function onOpen() {
  var ui = SpreadsheetApp.getUi();
  ui.createMenu("Send PRs")
    .addItem("Daily Quota Remaining", "dailyQ")
    .addSeparator()
    .addSubMenu(
      ui
        .createMenu("Phonics")
        .addItem("S3 Mid-Session Learning Report", "sendPS3MSLR")
    )
    .addSubMenu(
      ui
        .createMenu("Club Achievement")
        .addItem("S3 Mid-Session Learning Report", "sendCAS3MSLR")
    )
    .addSubMenu(
      ui
        .createMenu("Club One")
        .addItem("S3 Mid-Session Learning Report", "sendC1S3MSLR")
    )
    .addSubMenu(
      ui
        .createMenu("Club Two")
        .addItem("S3 Mid-Session Learning Report", "sendC2S3MSLR")
    )
    .addSubMenu(
      ui
        .createMenu("Club Three")
        .addItem("S3 Mid-Session Learning Report", "sendC3S3MSLR")
    )
    .addSubMenu(
      ui
        .createMenu("Club Four")
        .addItem("S3 Mid-Session Learning Report", "sendC4S3MSLR")
    )
    .addSubMenu(
      ui
        .createMenu("Club Essential")
        .addItem("S3 Mid-Session Learning Report", "sendCES3MSLR")
    )
    .addSubMenu(
      ui
        .createMenu("Club Five")
        .addItem("S3 Mid-Session Learning Report", "sendC5S3MSLR")
    )
    .addSubMenu(
      ui
        .createMenu("Club Six")
        .addItem("S3 Mid-Session Learning Report", "sendC6S3MSLR")
    )
    .addToUi();
}

/* CHECK IF ADMINISTRATION ACCOUNT'S DAILY QUOTA HAS BEEN REACHED */
function dailyQ() {
  var msg = MailApp.getRemainingDailyQuota();
  SpreadsheetApp.getActiveSpreadsheet().toast(msg, "Daily Quota Remaining:");
}

/*=======================================================================*/
/*==== MID-SESSION LEARNING REPORT ======================================*/
/*=======================================================================*/

function sendMSLR(
  e_header,
  e_text1,
  e_concept1,
  e_concept2,
  e_concept3,
  e_concept4,
  e_concept5,
  e_suggest1,
  e_suggest2,
  e_suggest3,
  e_suggest4,
  e_suggest5,
  e_session
) {
  //var sendCopy = "test@test.com"; // E-mail to send copies to
  var EMAIL_SENT = "E-mail sent.";
  var EMAIL_CHECK = "NO E-MAIL ADDRESS";
  var Code_One = "NR";
  var Code_Two = "NE";
  var Code_Three = "NF";

  var remaindq = MailApp.getRemainingDailyQuota(); // Quota Variable
  var sheet = SpreadsheetApp.getActiveSheet();
  var startRow = 3; // First row of data to process
  var numRows = sheet.getLastRow() - 2; // Number of rows to process

  // Fetch the range of cells: row, column, optNumRows, optNumColumns
  var dataRange = sheet.getRange(startRow, 1, numRows, 70);

  // Fetch values for each row in the Range.
  var data = dataRange.getValues();

  if (e_concept1 != "") {
    var e_concept1 = "<li>" + e_concept1 + "</li>";
  }
  if (e_concept2 != "") {
    var e_concept2 = "<li>" + e_concept2 + "</li>";
  }
  if (e_concept3 != "") {
    var e_concept3 = "<li>" + e_concept3 + "</li>";
  }
  if (e_concept4 != "") {
    var e_concept4 = "<li>" + e_concept4 + "</li>";
  }
  if (e_concept5 != "") {
    var e_concept5 = "<li>" + e_concept5 + "</li>";
  }

  if (e_suggest1 != "") {
    var e_suggest1 = "<li>" + e_suggest1 + "</li>";
  }
  if (e_suggest2 != "") {
    var e_suggest2 = "<li>" + e_suggest2 + "</li>";
  }
  if (e_suggest3 != "") {
    var e_suggest3 = "<li>" + e_suggest3 + "</li>";
  }
  if (e_suggest4 != "") {
    var e_suggest4 = "<li>" + e_suggest4 + "</li>";
  }
  if (e_suggest5 != "") {
    var e_suggest5 = "<li>" + e_suggest5 + "</li>";
  }

  var ui = SpreadsheetApp.getUi();
  var result = ui.alert(
    "Please confirm",
    "Are you sure you want to continue?",
    ui.ButtonSet.YES_NO
  );
  if (result == ui.Button.YES) {
    for (var i = 0; i < data.length; ++i) {
      var row = data[i];

      var emailAddress = row[0]; // Column where email addresses are
      //    if (emailAddress == 0) {
      //      sheet.getRange(startRow + i, 1).setValue(EMAIL_CHECK);
      //      SpreadsheetApp.flush();
      //    }

      var message =
        '<div style="width:600px; margin: 0 auto; text-align:center; color:#4A86E9"><img src="https://googledrive.com/host/0B_Q3DrHEUB8qX3JBZHVGX20wbG8/BFLCLogo-Jean" alt="BFLC Logo" height="81" width="400">' +
        '<p style="font-size:20px">' +
        e_header +
        '</p><p style="font-size:25px">' +
        row[2] +
        "</p></div><br>" +
        '<div style="width:600px; margin: 0 auto; text-align:justify">' +
        e_text1 +
        "</div><br>" +
        '<div align="center"><table style="width:600px; height:40px"><tr><th colspan="1" bgcolor="#4A86E9"><span style="color:#FFFFFF">Concepts Learned:</span></th></tr></table></div>' +
        '<div style="width:600px; margin: 0 auto; text-align:left"><ul>' +
        e_concept1 +
        e_concept2 +
        e_concept3 +
        e_concept4 +
        e_concept5 +
        "</ul></div>" +
        "<br/>" +
        '<div align="center"><table style="width:600px; height:40px"><tr><th colspan="1" bgcolor="#4A86E9"><span style="color:#FFFFFF">Suggestions for Continued Improvement</span></th></tr></table></div>' +
        '<div style="width:600px; margin: 0 auto; text-align:left"><ul>' +
        e_suggest1 +
        e_suggest2 +
        e_suggest3 +
        e_suggest4 +
        e_suggest5 +
        "</ul></div>" +
        "<br/>" +
        '<div align="center"><p style="font-weight:bold; font-size:18px">Have questions about your child’s learning?</p><p style="font-weight:bold; font-size:18px">Contact us at: www.theBFLC.com</p></div>' +
        "<br/>" +
        '<div align="center"><p>Brighter Future Learning Center</p><p>12175 Saratoga-Sunnyvale Rd. #B, Saratoga, CA 95070 ✰ www.theBFLC.com ✰ (408) 366 - 1129</p></div>';

      var emailSent = row[e_session - 1]; // Column where email sent is placed
      if (
        remaindq >= 3 &&
        emailAddress != 0 &&
        emailAddress != EMAIL_CHECK &&
        emailSent != EMAIL_SENT &&
        emailSent != Code_One &&
        emailSent != Code_Two &&
        emailSent != Code_Three
      ) {
        // Duplicate Send Guard, Blank Email Check, Quota Check
        var subject = row[2] + "'s " + e_header;
        MailApp.sendEmail({
          to: emailAddress,
          subject: subject,
          htmlBody: message,
        });
        //MailApp.sendEmail({to:sendCopy, subject:subject, htmlBody:message,});
        sheet.getRange(startRow + i, e_session).setValue(EMAIL_SENT); // Column number where email sent is placed
        SpreadsheetApp.flush();
      }
    }
    if (remaindq < 3) {
      ui.alert("Safe Quota Count Reached");
    }
  } else {
    ui.alert("Sending cancelled.");
  }
}

/*=======================================================================*/
/*==== END OF SESSION LEARNING REPORT ===================================*/
/*=======================================================================*/

function sendEoSLR(
  e_header,
  e_text1,
  e_concept1,
  e_concept2,
  e_concept3,
  e_concept4,
  e_concept5,
  e_suggest1,
  e_suggest2,
  e_suggest3,
  e_suggest4,
  e_suggest5
) {
  //var sendCopy = "test@test.com"; // E-mail to send copies to
  var EMAIL_SENT = "E-mail sent.";
  var EMAIL_CHECK = "NO E-MAIL ADDRESS";
  var Code_One = "NR";
  var Code_Two = "NE";
  var Code_Three = "NF";

  var remaindq = MailApp.getRemainingDailyQuota(); // Quota Variable
  var sheet = SpreadsheetApp.getActiveSheet();
  var startRow = 3; // First row of data to process
  var numRows = sheet.getLastRow() - 2; // Number of rows to process
  // Fetch the range of cells: row, column, optNumRows, optNumColumns
  var dataRange = sheet.getRange(startRow, 1, numRows, 42);
  // Fetch values for each row in the Range.
  var data = dataRange.getValues();

  if (e_concept1 != "") {
    var e_concept1 = "<li>" + e_concept1 + "</li>";
  }
  if (e_concept2 != "") {
    var e_concept2 = "<li>" + e_concept2 + "</li>";
  }
  if (e_concept3 != "") {
    var e_concept3 = "<li>" + e_concept3 + "</li>";
  }
  if (e_concept4 != "") {
    var e_concept4 = "<li>" + e_concept4 + "</li>";
  }
  if (e_concept5 != "") {
    var e_concept5 = "<li>" + e_concept5 + "</li>";
  }

  if (e_suggest1 != "") {
    var e_suggest1 = "<li>" + e_suggest1 + "</li>";
  }
  if (e_suggest2 != "") {
    var e_suggest2 = "<li>" + e_suggest2 + "</li>";
  }
  if (e_suggest3 != "") {
    var e_suggest3 = "<li>" + e_suggest3 + "</li>";
  }
  if (e_suggest4 != "") {
    var e_suggest4 = "<li>" + e_suggest4 + "</li>";
  }
  if (e_suggest5 != "") {
    var e_suggest5 = "<li>" + e_suggest5 + "</li>";
  }

  var ui = SpreadsheetApp.getUi();
  var result = ui.alert(
    "Please confirm",
    "Are you sure you want to continue?",
    ui.ButtonSet.YES_NO
  );
  if (result == ui.Button.YES) {
    for (var i = 0; i < data.length; ++i) {
      var row = data[i];

      var emailAddress = row[0]; // Column where email addresses are
      if (emailAddress == 0) {
        sheet.getRange(startRow + i, 1).setValue(EMAIL_CHECK);
        SpreadsheetApp.flush();
      }

      var message =
        '<div style="width:600px; margin: 0 auto; text-align:center; color:#4A86E9"><img src="https://googledrive.com/host/0B_Q3DrHEUB8qX3JBZHVGX20wbG8/BFLCLogo-Jean" alt="BFLC Logo" height="81" width="400">' +
        '<p style="font-size:20px">' +
        e_header +
        '</p><p style="font-size:25px">' +
        row[2] +
        "</p></div><br>" +
        '<div style="width:600px; margin: 0 auto; text-align:justify">' +
        e_text1 +
        "</div><br>" +
        '<div align="center"><table style="width:600px; height:40px"><tr><th colspan="1" bgcolor="#4A86E9"><span style="color:#FFFFFF">Concepts Learned:</span></th></tr></table></div>' +
        '<div style="width:600px; margin: 0 auto; text-align:left"><ul>' +
        e_concept1 +
        e_concept2 +
        e_concept3 +
        e_concept4 +
        e_concept5 +
        "</ul></div>" +
        "<br/>" +
        '<div align="center"><table style="width:600px; height:40px"><tr><th colspan="1" bgcolor="#4A86E9"><span style="color:#FFFFFF">Suggestions for Continued Improvement</span></th></tr></table></div>' +
        '<div style="width:600px; margin: 0 auto; text-align:left"><ul>' +
        e_suggest1 +
        e_suggest2 +
        e_suggest3 +
        e_suggest4 +
        e_suggest5 +
        "</ul></div>" +
        "<br/>" +
        '<div align="center"><p style="font-weight:bold; font-size:18px">Have questions about your child’s learning?</p><p style="font-weight:bold; font-size:18px">Contact us at: www.theBFLC.com</p></div>' +
        "<br/>" +
        '<div align="center"><p>Brighter Future Learning Center</p><p>12175 Saratoga-Sunnyvale Rd. #B, Saratoga, CA 95070 ✰ www.theBFLC.com ✰ (408) 366 - 1129</p></div>';

      var emailSent = row[11]; // Column where email sent is placed
      if (
        remaindq != 0 &&
        emailAddress != 0 &&
        emailAddress != EMAIL_CHECK &&
        emailSent != EMAIL_SENT &&
        emailSent != Code_One &&
        emailSent != Code_Two &&
        emailSent != Code_Three
      ) {
        // Duplicate Send Guard, Blank Email Check, Quota Check
        var subject = row[2] + "'s " + e_header;
        MailApp.sendEmail({
          to: emailAddress,
          subject: subject,
          htmlBody: message,
        });
        //MailApp.sendEmail({to:sendCopy, subject:subject, htmlBody:message,});
        sheet.getRange(startRow + i, 12).setValue(EMAIL_SENT); // Column number where email sent is placed
        SpreadsheetApp.flush();
      }
    }
    if (remaindq == 0) {
      ui.alert("Maximum Quota Count Reached");
    }
  } else {
    ui.alert("Sending cancelled.");
  }
}

var message =
  '<div style="width:600px; margin: 0 auto; text-align:center; color:#4A86E9; font-size:25px"><p>Session Four Final Report<br>' +
  row[11] +
  "</p></div><br>" +
  '<div style="font-style:italic; font-weight:bold; width:600px; margin: 0 auto; text-align:right">10-8 = Mastered, 5-7 = Proficient, 4-1 = Needs Improvement, N/A = Conference Required</div>' +
  '<div align="center"><table style="border:2px solid black;width:600px" >' +
  '<tr><th colspan="1" bgcolor="#4A86E9"><span style="color:#FFFFFF">Concepts</span></th><th colspan="1" bgcolor="#4A86E9"><span style="color:#FFFFFF">Learning Progress</span></th></tr>' +
  '<tr colspan="2"><td>Writes Instructions explaining how to do a specific task</td><td><div align="center">' +
  row[7] +
  "</div></td></tr>" +
  '<tr colspan="2"><td>Provides a list of materials and steps instructing how to do the task</td><td><div align="center">' +
  row[8] +
  "</div></td></tr>" +
  '<tr colspan="2"><td>Uses Time Order Words</td><td><div align="center">' +
  row[9] +
  "</div></td></tr>" +
  '<tr colspan="2"><td>Demonstrates an understanding of Vocabulary Word Usage</td><td><div align="center">' +
  row[10] +
  "</div></td></tr>" +
  '<tr colspan="2"><td>Identifies Linking, Transitive, and Intransitive Verbs</td><td><div align="center">' +
  row[7] +
  "</div></td></tr>" +
  '<tr colspan="2"><td>Identifies Adverbs in sentences and how to use correct punctuation with Adverbs</td><td><div align="center">' +
  row[8] +
  "</div></td></tr>" +
  '<tr colspan="2"><td>Understands how to label Complements and identify the Form of Speech</td><td><div align="center">' +
  row[9] +
  "</div></td></tr>" +
  "<tr colspan=\"2\"><td>Understands how to correctly use the tricky verbs 'to lay' and 'to lie.'</td><td><div align=\"center\">" +
  row[10] +
  "</div></td></tr>" +
  "</table><br/>" +
  '<table style="border:2px solid black;width:600px" >' +
  '<tr><th bgcolor="#4A86E9"><span style="color:#FFFFFF">Teacher\'s Comments</span></th></tr>' +
  "<td>" +
  row[2] +
  "</td>" +
  "</table></div><br/>" +
  '<div style="width:600px; margin: 0 auto"><p style="font-weight:bold">Coming up in Session Application</p>' +
  '<ul><li>Reading Comprehension Discussions</li><li>Grammar Application Practice</li><li>Writing Application Practice</li><li>Key Vocabulary Terminology Practice</li><li style="list-style-type:none">...and more!</li></ul>' +
  "</div>" +
  '<div align="center">' +
  '<p style="color:#E0C880">Brighter Future Learning Center</p><p style="color:#E0C880">12175 Saratoga-Sunnyvale Rd. #B, Saratoga, CA 95070 ✫ www.theBFLC.com ✫ (408) 366 - 1129</p>' +
  "</div>"; // Second column

/*=======================================================================*/
/*==== SESSION 3 ========================================================*/
/*=======================================================================*/

function sendPS3MSLR() {
  var grid = SpreadsheetApp.getActiveSpreadsheet()
    .getSheetByName(templatename)
    .getRange(51, 2, 65, 2)
    .getValues();
  sendMSLR(
    grid[0][0],
    grid[2][0],
    grid[4][0],
    grid[5][0],
    grid[6][0],
    grid[7][0],
    grid[8][0],
    grid[10][0],
    grid[11][0],
    grid[12][0],
    grid[13][0],
    grid[14][0],
    S3MidSentCol
  );
}

function sendCAS3MSLR() {
  var grid = SpreadsheetApp.getActiveSpreadsheet()
    .getSheetByName(templatename)
    .getRange(51, 5, 65, 5)
    .getValues();
  sendMSLR(
    grid[0][0],
    grid[2][0],
    grid[4][0],
    grid[5][0],
    grid[6][0],
    grid[7][0],
    grid[8][0],
    grid[10][0],
    grid[11][0],
    grid[12][0],
    grid[13][0],
    grid[14][0],
    S3MidSentCol
  );
}

function sendC1S3MSLR() {
  var grid = SpreadsheetApp.getActiveSpreadsheet()
    .getSheetByName(templatename)
    .getRange(51, 8, 65, 8)
    .getValues();
  sendMSLR(
    grid[0][0],
    grid[2][0],
    grid[4][0],
    grid[5][0],
    grid[6][0],
    grid[7][0],
    grid[8][0],
    grid[10][0],
    grid[11][0],
    grid[12][0],
    grid[13][0],
    grid[14][0],
    S3MidSentCol
  );
}

function sendC2S3MSLR() {
  var grid = SpreadsheetApp.getActiveSpreadsheet()
    .getSheetByName(templatename)
    .getRange(51, 11, 65, 11)
    .getValues();
  sendMSLR(
    grid[0][0],
    grid[2][0],
    grid[4][0],
    grid[5][0],
    grid[6][0],
    grid[7][0],
    grid[8][0],
    grid[10][0],
    grid[11][0],
    grid[12][0],
    grid[13][0],
    grid[14][0],
    S3MidSentCol
  );
}

function sendC3S3MSLR() {
  var grid = SpreadsheetApp.getActiveSpreadsheet()
    .getSheetByName(templatename)
    .getRange(51, 14, 65, 14)
    .getValues();
  sendMSLR(
    grid[0][0],
    grid[2][0],
    grid[4][0],
    grid[5][0],
    grid[6][0],
    grid[7][0],
    grid[8][0],
    grid[10][0],
    grid[11][0],
    grid[12][0],
    grid[13][0],
    grid[14][0],
    S3MidSentCol
  );
}

function sendC4S3MSLR() {
  var grid = SpreadsheetApp.getActiveSpreadsheet()
    .getSheetByName(templatename)
    .getRange(51, 17, 65, 17)
    .getValues();
  sendMSLR(
    grid[0][0],
    grid[2][0],
    grid[4][0],
    grid[5][0],
    grid[6][0],
    grid[7][0],
    grid[8][0],
    grid[10][0],
    grid[11][0],
    grid[12][0],
    grid[13][0],
    grid[14][0],
    S3MidSentCol
  );
}

function sendCES3MSLR() {
  var grid = SpreadsheetApp.getActiveSpreadsheet()
    .getSheetByName(templatename)
    .getRange(51, 20, 65, 20)
    .getValues();
  sendMSLR(
    grid[0][0],
    grid[2][0],
    grid[4][0],
    grid[5][0],
    grid[6][0],
    grid[7][0],
    grid[8][0],
    grid[10][0],
    grid[11][0],
    grid[12][0],
    grid[13][0],
    grid[14][0],
    S3MidSentCol
  );
}

function sendC5S3MSLR() {
  var grid = SpreadsheetApp.getActiveSpreadsheet()
    .getSheetByName(templatename)
    .getRange(51, 23, 65, 23)
    .getValues();
  sendMSLR(
    grid[0][0],
    grid[2][0],
    grid[4][0],
    grid[5][0],
    grid[6][0],
    grid[7][0],
    grid[8][0],
    grid[10][0],
    grid[11][0],
    grid[12][0],
    grid[13][0],
    grid[14][0],
    S3MidSentCol
  );
}

function sendC6S3MSLR() {
  var grid = SpreadsheetApp.getActiveSpreadsheet()
    .getSheetByName(templatename)
    .getRange(51, 26, 65, 26)
    .getValues();
  sendMSLR(
    grid[0][0],
    grid[2][0],
    grid[4][0],
    grid[5][0],
    grid[6][0],
    grid[7][0],
    grid[8][0],
    grid[10][0],
    grid[11][0],
    grid[12][0],
    grid[13][0],
    grid[14][0],
    S3MidSentCol
  );
}
